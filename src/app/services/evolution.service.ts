import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EvolutionService {

  constructor(private http: HttpClient) { }

  private url = 'https://pokeapi.co/api/v2/evolution-chain/'

  getPokemonEvolutionById(url: string) {
    return this.http.get<any>(url);
  }
}
