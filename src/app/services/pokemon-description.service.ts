import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PokemonDescriptionService {

  private url = 'https://pokeapi.co/api/v2/pokemon-species/'

  constructor(private http: HttpClient) { }

  getPokemonDescriptionById(id: number) {
    return this.http.get<any>(this.url + id);
  }
}
