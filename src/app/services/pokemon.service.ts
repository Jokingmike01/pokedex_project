import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PokemonDetails } from '../models/pokemonDetails';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private url = 'https://pokeapi.co/api/v2/pokemon/'

  constructor(private http: HttpClient) { }

  getPokemonbyId(id: number) {
    return this.http.get<PokemonDetails>(this.url + id);
  }
}
