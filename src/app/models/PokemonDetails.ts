import { Version } from '@angular/core';

export interface PokemonDetails {
    id: number;
    abilities: Ability[];
    name: string;
    base_experience: number;
    sprites: Sprite;
    stats: Stat[];
    moves: Move[];
    types: Type[];
}

export interface Sprite {
    front_default: string;
    other: OtherImg;
}

export interface OtherImg {
    dream_world: DreamWorldImg;
}

export interface DreamWorldImg {
    front_default: string;
}

export interface Ability {
    ability: DetailInfo[]
}

export interface Move {
    move: DetailInfo[];
    
}

export interface Type {
    type: DetailInfo[];
    
}

export interface Stat {
    base_stat: number;
    stat: DetailInfo[];
}

export interface DetailInfo{
    name: string;
    url: string;
}