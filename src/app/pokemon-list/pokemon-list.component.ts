import { Component, OnInit, ViewChild } from '@angular/core';
import { PokemonDetails } from '../models/pokemonDetails';
import { PokemonService } from '../services/pokemon.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PokemonDescriptionService } from '../services/pokemon-description.service';
import { EvolutionService } from '../services/evolution.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded => collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
      transition('collapsed => expanded', animate('225ms 0.5s cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class PokemonListComponent implements OnInit {

  pokemonlist: PokemonDetails[] = [];
  pokemonNumber: number = 250; //change this number to add more (or remove) pokemons from the table list
  datasource = new MatTableDataSource<PokemonDetails>(this.pokemonlist);
  evolves_to: any;
  pokemonDetails: any;
  pokemonEvolutionArr: string[];

  displayedColumns: string[] = ['#', 'image', 'name', 'exp', 'hp', 'attack', 'defense', 'sp-attack', 'sp-defense', 'speed', " "];
  expandedElement: PokemonDetails | null;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private pokemonService: PokemonService,
    private pokemonDescriptionService: PokemonDescriptionService,
    private evolutionService: EvolutionService) { }

  ngOnInit(): void {
    this.getPokemons();
  }

  getColor(type) {
    switch (type) {
      case 'fire':
        return 'tomato';
      case 'grass':
        return 'greenyellow';
      case 'electric':
        return 'yellow';
      case 'water':
        return 'aqua';
      case 'ground':
        return '#f4e7da';
      case 'rock':
        return 'brown';
      case 'fairy':
        return '#fceaff';
      case 'poison':
        return 'violet';
      case 'bug':
        return '#f8d5a3';
      case 'dragon':
        return '#97b3e6';
      case 'psychic':
        return '#eaeda1';
      case 'flying':
        return '#F5F5F5';
      case 'fighting':
        return '#E6E0D4';
    }
  }

  getPokemons() {
    let pokemonData;

    for (let i = 1; i <= this.pokemonNumber; i++) {
      this.pokemonService.getPokemonbyId(i)
        .subscribe(response => {
          pokemonData = {
            id: response.id,
            abilities: response.abilities,
            name: response.name,
            stats: response.stats,
            base_experience: response.base_experience,
            moves: response.moves,
            types: response.types,
            sprites: response.sprites
          }
          this.pokemonlist.push(pokemonData);
          this.datasource = new MatTableDataSource<PokemonDetails>(this.pokemonlist.sort((a, b) => (a.id > b.id) ? 1 : -1));
          this.datasource.paginator = this.paginator;
          this.datasource.sort = this.sort;
          //console.log(this.pokemonlist);
        }, err => {
          console.log(err)
        })
    }
  }

  getPokemonDescription(id: number) {
    this.pokemonDescriptionService.getPokemonDescriptionById(id)
      .subscribe(response => {
        this.pokemonDetails = response;
        console.log(this.pokemonDetails);
        this.evolutionService.getPokemonEvolutionById(this.pokemonDetails.evolution_chain.url)
          .subscribe(response => {
            this.getPokemonEvolution(response);
          }, err => {
            console.log(err)
          })
      }, err => {
        console.log(err)
      })
  }

  getPokemonEvolution(response: any) {
    this.pokemonEvolutionArr = [];
    if (response.chain.species) { this.pokemonEvolutionArr.push(response.chain.species.name); }
    if (response.chain.evolves_to[0]) {
      this.pokemonEvolutionArr.push(response.chain.evolves_to[0].species.name);
      if (response.chain.evolves_to[0].evolves_to[0]) { this.pokemonEvolutionArr.push(response.chain.evolves_to[0].evolves_to[0].species.name); }
    }
    this.evolves_to = null;
    for (var i = 0; i < this.pokemonEvolutionArr.length; i++) {
      if (this.pokemonEvolutionArr[i] == this.pokemonDetails.name && this.pokemonEvolutionArr[i + 1]) {
        this.evolves_to = this.pokemonlist.find(pokemon => pokemon.name == this.pokemonEvolutionArr[i + 1]);
      }
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.datasource.filter = filterValue.trim().toLowerCase();

    if (this.datasource.paginator) {
      this.datasource.paginator.firstPage();
    }
  }
}
