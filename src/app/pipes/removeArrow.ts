import {Pipe, PipeTransform} from '@angular/core';
@Pipe ({
   name : 'removeArrow'
})
export class RemoveArrow implements PipeTransform {
   transform(val : string) : string {
      return val.replace(//g, ' ');
   }
}